# Issues

Issues of the International Journal of Proof-of-concept or Get the F*** out!

Each hyperlink will take you to the selected issue or, you could just clone the whole repo if you'd like.

## 2013
- [Issue 0x00](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo00.pdf) - August 2013 _An epistle from the desk of Rt. Revd. Pastor Manul Laphroaig_
- [Issue 0x01](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo01.pdf) - October 2013 _an Epistle to the 10th H2HC in São Paulo
From the writing desk, not the raven, of Rt. Revd. Preacherman Pastor Manul Laphroaig_
- [Issue 0x02](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo02.pdf) - December 2013 _an Epistle to the 30th CCC Congress in Hamburg_
## 2014
- [Issue 0x03]((https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo03.pdf)) - March 2014, _An Address to the Secret Society of PoC||GTFO concerning the Gospel of the Weird Machines and also the Smashing of Idols to Bits and Bytes by the Rt. Revd. Dr. Pastor Manul Laphroaig_
- [Issue 0x04](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo04.pdf) - June 2014, _Tract de la Société Secréte de PoC||GTFO sur l'évangile des machines éstranges et autres stjets techniques par le prédicateur Pasteur Manul Laphroaig_
- [Issue 0x05](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo05.pdf) - August 2014, _addressed to the INHABITANTS of EARTH on the following and other INTERESTING SUBJECTS written for the edification of ALL GOOD NEIGHBORS_
- [Issue 0x06](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo06.pdf) - November 2014, PoC||GTFO brings that OLD TIMEY EXPLOITATION with a WEIRD MACHINE JAMBOREE and our world-famous FUNKY FILE FLEA MARKET not to be ironic, but because WE LOVE THE MUSIC!
## 2015
- [Issue 0x07](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo07.pdf) - March 2015, _PoC||GTFO, CALISTHENICS & ORTHODONTIA in remembrance OF OUR BELOVED DR. DOBB because THE WORLD IS ALMOST THROUGH!_
- [Issue 0x08](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo08.pdf) - June 2015, _As exploits sit lonely, FORGOTTEN ON THE SHELF your friendly neighbors at PoC||GTFO proudly present PASTOR MANUL LAPHROAIG'S export-controlled CHURCH NEWSLETTER_
- [Issue 0x09](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo09.pdf) - September 2015, _PASTOR MANUL LAPHROAIG'S tabernacle choir SINGS REVERENT ELEGIES of the SECOND CRYPTO WAR_
## 2016
- [Issue 0x10](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo10.pdf) - January 2016, _IN THE THEATER OF LITERATE DISASSEMBLY, PASTOR MANUL LAPHROAIG AND HIS MERRY BAND OF REVERSE ENGINEERS LIFT THE WELDED HOOD FROM THE ENGINE THAT RUNS THE WORLD!_
- [Issue 0x11](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo11.pdf) - March 2016, _IN A FIT OF STUBBORN OPTIMISM, PASTOR MANUL LAPHROAIG AND HIS CLEVER CREW SET SAIL TOWARD WELCOMING SHORES OF THE GREAT UNKNOWN!_
- [Issue 0x12](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo12.pdf) - June 2016, _COLLECTING BOTTLES OF BROKEN THINGS, PASTOR MANUL LAPHROAIG WITH THEORY AND PRAXIS COULD BE THE MAN WHO SNEAKS A LOOK BEHIND THE CURTAIN!_
- [Issue 0x13](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo13.pdf) - October 2016, _PASTOR LAPHROAIG'S MERCY SHIP HOLDS STONES FROM THE IVORY TOWER, BUT ONLY AS BALLAST!_
## 2017
- [Issue 0x14](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo14.pdf) - March 2017, _PASTOR LAPHROAIG SCREAMS HIGH FIVE TO THE HEAVENS AS THE WHOLE WORLD GOES UNDER_
- [Issue 0x15](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo15.pdf) - June 2017, _I SLIPPED A LITTLE BUT LAPHROAIG WAS THERE WITH A HELPING HAND, A NIFTY IDEA AND TWO LITERS OF COFFEE_
- [Issue 0x16](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo16.pdf) - October 2017, _PASTOR LAPHROAIG RACES THE RUNTIME RELINKER AND OTHER TRUE TALES OF CLEVERNESS AND CRAFT_
- [Issue 0x17](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo17.pdf) - December 2017, _It's damned cold outside, so let's light ourselves a fire! warm ourselves with whiskey! and teach ourselves some tricks!_
## 2018
- [Issue 0x18](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo18.pdf) - June 2018, _Pastor Manul Laphroaig's Montessori Soldering School and Stack Smashing Academy for Youngsters Gifted and Not_
## 2019
- [Issue 0x19](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo19.pdf) - March 2019, _This janky old piano has a few more tunes! And so do you! And so do I!_
## 2020
- [Issue 0x20](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo20.pdf) - January 2020, _Grab gifts from the genizah, reading every last page! And write in their margins! And give them all again!_
## 2022
- [Issue 0x21](https://gitlab.com/pocorgtfo/issues/-/blob/main/Files/pocorgtfo21.pdf) - February 2022, _Notebook of Altera NIOS Disassembly, Routable IPIP Spoofing, PCAP-NG Polyglots, Weird Machinery, Code Golfing, and UHF-VHF Tuners_

